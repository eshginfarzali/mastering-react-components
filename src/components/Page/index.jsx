import { Footer } from "../Footer"
import { Header } from "../Header"
import { Main } from "../Main"

export const Page = () => {
  return (
    <>
    <Header title={"Header title"}/>
    <Main/>
    <Footer title={"Footer"}/>
    </>
  )
}
