import PropTypes from 'prop-types';

export const Header = ({ title = 'Header title' }) => {
  return <header>{title}</header>;
};

Header.propTypes = {
  title: PropTypes.string,
};


