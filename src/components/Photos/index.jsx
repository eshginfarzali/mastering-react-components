/*eslint-disable*/

export const Photos = ({ data }) => {
  return (
    <div className="photos">
      {data?.map((photo) => (
        <div key={photo.id} className="photo">
          <img src={photo.thumbnailUrl} alt={photo.title} />
          <p>{photo.title}</p>
        </div>
      ))}
    </div>
  );
};

